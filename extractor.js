javascript:(
    function() {
        const hidePageStyle = document.createElement('style');
        hidePageStyle.innerHTML = 'body > :not(.iHateCostpointElement) {display: none;}';

        window.orderList = [];
        var addr = window.location
        addr = addr.toString().split('?')[0] //mainly for amazon
        switch (addr) {
            case 'https://www.amazon.com/gp/cart/view.html': window.orderList=extractAmazon(); break;
            case 'https://www.digikey.com/ordering/shoppingcart': window.orderList=extractDigikey(); break;
            case 'https://www.mouser.com/Cart/': window.orderList=extractMouser(); break;
            case 'https://www.mcmaster.com/orders': window.orderList=extractMcmaster(); break;
            case 'https://www.thorlabs.com/cart/basket/Showcott3.cfm': window.orderList=extractThorlabs(); break;
            default: alert('Site not supported. Do it the old way, sucka.'); return;
            }
        console.log(window.orderList.join("<br>\n"));
        
        document.head.appendChild(hidePageStyle); //disappear page
        instructions = document.createElement('h4');
        instructions.className = "iHateCostpointElement";
        instructions.id = 'instructions';
        instructions.innerText = 'Simplified order list (you can paste this into Excel if you want).';
        document.body.appendChild(instructions);
        
        textData = document.createElement('textarea');
        textData.className = "iHateCostpointElement";
        textData.id = 'csvdata';
        textData.value = window.orderList.join('\n');
        textData.style.width = "800px";
        textData.style.height = 'auto'; //window.orderList.length*100;
        document.body.appendChild(textData);
        createFormFields();
    
    function addBusinessDays(n) {
        d = new Date();
        var day = d.getDay();
        d.setDate(d.getDate() + n + (day === 6 ? 2 : +!day) + (Math.floor((n - 1 + (day % 6 || 1)) / 5) * 2));
        return d.toLocaleDateString();
    }
    function extractThorlabs() {
        var data = document.querySelectorAll("table[id='t1']>tbody>tr");
        var buf = [ ["Thorlabs", "Part Number", "Price", "Quantity", "Description", "MPN", "ExtPrice"] ];
        try {
            for (let i=0; i < data.length; ++i) {
                var lineNo = i+1;
                var price = data[i].querySelectorAll("td")[5].innerText;
                var quantity = data[i].querySelector("input[class='qtyUpdate']").value
                var vpn = data[i].querySelector("td font b a").innerText;
                var description_block = data[i].querySelectorAll("td")[2].innerText.split('\n');
                var description = "";
                for (let j=0; j < description_block.length; ++j) {
                    if (j == 0) continue;
                    if (description_block[j] != "") description += description_block[j];
                }
                var link = data[i].querySelector('td font b a').href
                var extPrice = data[i].querySelectorAll("td")[6].innerText
                buf.push([lineNo, vpn, price, quantity, description, link, extPrice]);
            }
            return buf;
        } catch(err) {
            alert("Error: "+err);
            return;
        }
    }
    function extractDigikey() {
        var data = document.querySelectorAll("table[id='cartDetails']>tbody>tr.detailRow:not(.subRow)");
        var buf = [ ["Digikey", "Part Number", "Price", "Quantity", "Description", "MPN", "ExtPrice"] ];
        var i = 1;
        try {
            data.forEach(
                function(node) {
                    var lineNo = i++;
                    var price = node.querySelector("div.cart-unitPrice").innerText;
                    var extPrice = node.querySelector("div.cart-extendedPrice").innerText;
                    var quantity = node.getElementsByClassName('part-quantity')[0].value;
                    var vpn = node.querySelector("div.cart-partNumber").innerText;
                    var mpn = node.querySelector("div.cart-mfgPartNumber").innerText;
                    var description = node.querySelector("div.cart-description").innerText;
                    buf.push([lineNo, vpn, price, quantity, description, mpn, extPrice]);
                }
            );
            return buf;
        } catch(err) {
            alert("Error: "+err);
            return;
        }
    }
    function extractMouser() {
        var data = document.querySelectorAll("tbody[class='cart-body']>tr.grid-row");
        var buf = [ ["Mouser", "Part Number", "Price", "Quantity", "Description", "MPN", "ExtPrice"] ];
        var i = 1;
        try {
            data.forEach(
                function(node) {
                    var lineNo = i++;
                    var price    = node.querySelector('td.col-unitprice>div.row>div.col-sm-12').innerText;
                    var extPrice = node.querySelector('td.col-ext>div.row>div.col-sm-12').innerText;
                    var quantity = node.querySelector('.jsQuantityInput').value;
                    var vpn      = node.getAttribute('data-partnumber');
                    var mpn      = node.querySelector('td.grid-cell.col-1.collapse-xs div.row div.col-xs-10.col-sm-10 div.row div.col-xs-7.col-sm-7').innerText;
                    var description = node.querySelector('td.grid-cell.hide-xs.collapse-xs.col-desc div.row div.col-xs-12.col-sm-12').innerText;
                    buf.push([lineNo, vpn, price, quantity, description, mpn, extPrice]);
                    console.log(buf);
                }
            );
            return buf;
        } catch(err) {
            alert("Error: "+err);
            return;
        }
    }
    function extractMcmaster() {
        data = document.querySelectorAll('div.order-pad-line');
        var buf = [ ["McMaster-Carr", "Part Number", "Price", "Quantity", "Description", "MPN", "ExtPrice"] ];
        var i = 1;
        try {
            data.forEach(
                function(node) {
                    var lineNo   = node.querySelector('div.line-number').innerText;
                    var price    = node.querySelector('div.line-unit-price>div').innerText;
                    var extPrice = node.querySelector('div.line-total-price').innerText;
                    var quantity = node.querySelector('input.line-quantity-input').value;
                    var vpn      = node.querySelector('input.line-part-number-input').value;
                    var mpn      = node.querySelector('div.details-web--view').innerText; //description details
                    var description = node.querySelector('div.title-text').innerText;
                    buf.push([lineNo, vpn, price, quantity, description, mpn, extPrice]);
                    console.log(buf);
                }
            );
            return buf;
        } catch(err) {
            alert("Error: "+err);
            return;
        }
    }
    function extractAmazon() {
        var data = document.querySelectorAll("div[data-name='Active Items'] div[data-asin]");
        var buf = [ ["Amazon Line Number", "ProductID", "Price", "Quantity", "Description", "Link", "ExtPrice"] ];
        var i = 1; //line number
        try	{
            data.forEach(
                function(node) {
                    var price = node.getAttribute("data-price");
                    var quantity = node.getAttribute("data-quantity");
                    var productID = node.getAttribute("data-asin");
                    var link = "http://amazon.com/dp/" + productID;
                    var title = '"'+node.querySelector(".sc-product-title").textContent.replace(/"/g, '""').trim() + '"';
                    buf.push([i++, productID, price, quantity, title, link, price*quantity]);
                }
            );
            return buf;
        } catch(err) {
            alert("Error: "+err);
            return;
        }
    }
    function createFormFields() {
        labelDeliverTo = document.createElement('label');
        labelDeliverTo.innerText = 'Deliver To:';
        labelDeliverTo.className = "iHateCostpointElement";
        fieldDeliverTo = document.createElement('input');
        fieldDeliverTo.className = "iHateCostpointElement";
        fieldDeliverTo.setAttribute('id', 'deliverTo');
        fieldDeliverTo.setAttribute('type', 'text');
        fieldDeliverTo.setAttribute('placeholder', 'Name of person to deliver this product to...');
        fieldDeliverTo.style.width = "300px";
        //fieldDeliverTo.value = 'DEFAULTNAME';
        var br = document.createElement("br");
        br.className = "iHateCostpointElement";

        labelNeededDate = document.createElement('label');
        labelNeededDate.innerText = 'Needed Date:';
        labelNeededDate.className = "iHateCostpointElement";
        fieldNeededDate = document.createElement('input');
        fieldNeededDate.setAttribute('id', 'neededDate');
        fieldNeededDate.setAttribute('type', 'text');
        fieldNeededDate.setAttribute('placeholder', 'Date item needed in house');
        fieldNeededDate.value = addBusinessDays(1);
        fieldNeededDate.style.width = "300px";
	
	labelItemType = document.createElement('label');
	labelItemType.innerText = 'Item type:';
	labelItemType.className = "iHateCostpointElement";
	itemTypes = [ 'G-LAB EQUIPMENT - COMPONENTS',
		      'G-LAB EQUIPMENT - ELECTRONICS',
		      'G-LAB EQUIPMENT - TOOLS',
		      'G-HARDWARE' 
	            ];
	fieldItemType = document.createElement('select');
	fieldItemType.className = "iHateCostpointElement";
	fieldItemType.id = 'itemType';
	for (i=0; i < itemTypes.length; ++i) addOptionToSelect(fieldItemType, itemTypes[i]);

        labelDeliverySite = document.createElement('label');
        labelDeliverySite.innerText = 'Office Address Code:';
        labelDeliverySite.className = "iHateCostpointElement";
        deliverySiteCodes = [ ['DC', 'Arlington, VA'],
                              ['AZ', 'Tucson, AZ'],
                              ['CO', 'Longmont, CO'],
                              ['VA', 'Chantilly, VA']
                            ];
        fieldDeliverySite = document.createElement('select');
        for (i = 0; i < deliverySiteCodes.length; ++i) addOptionToSelect(fieldDeliverySite, deliverySiteCodes[i][0]);
        fieldDeliverySiteDetail = document.createElement('label');
        fieldDeliverySiteDetail.id = 'deliverySite';
        fieldDeliverySiteDetail.className = "iHateCostpointElement";
        fieldDeliverySite.className = "iHateCostpointElement";
        fieldDeliverySite.addEventListener("change", function() {
            // update label to show shipping location description
            document.getElementById('deliverySite').innerText = deliverySiteCodes[fieldDeliverySite.selectedIndex][1];
            } 
        );

        document.body.appendChild(br.cloneNode());
        document.body.appendChild(labelDeliverTo);
        labelDeliverTo.appendChild(fieldDeliverTo);
        document.body.appendChild(br.cloneNode());

        document.body.appendChild(labelNeededDate);
        labelNeededDate.appendChild(fieldNeededDate);
        document.body.appendChild(br.cloneNode());

        document.body.appendChild(labelDeliverySite);
        labelDeliverySite.appendChild(fieldDeliverySite);
        labelDeliverySite.appendChild(fieldDeliverySiteDetail);
	
        document.body.appendChild(br.cloneNode());
	document.body.appendChild(labelItemType);
	labelItemType.appendChild(fieldItemType);
        document.body.appendChild(br.cloneNode());
	
        // set label initially
        document.getElementById('deliverySite').innerText = deliverySiteCodes[fieldDeliverySite.selectedIndex][1];
        document.body.appendChild(br.cloneNode());
        
        button = document.createElement('button');
        button.id = 'copy';
        button.className = "iHateCostpointElement";
        button.innerText = 'Click here to copy Costpoint data to clipboard.';
        button.onclick = copyCostpointDataToClipboard;
        document.body.appendChild(button);
        button.focus()
    }
    function addOptionToSelect(sel, optionText) {
        opt = document.createElement('option');
        opt.text = optionText;
        sel.add(opt);
    }
    function copyCostpointDataToClipboard() {
        console.log('copytoclip');
        console.log(window.orderList);
        deliverTo = document.getElementById('deliverTo').value;
        neededDate = document.getElementById('neededDate').value;
        deliverySite = deliverySiteCodes[fieldDeliverySite.selectedIndex][0];
	itemType = document.getElementById('itemType').value;

        cpData = createReqLines(window.orderList, deliverTo, neededDate, deliverySite, itemType);
        
        labelDeliverTo.parentNode.removeChild(labelDeliverTo);
        labelDeliverySite.parentNode.removeChild(labelDeliverySite);
        labelNeededDate.parentNode.removeChild(labelNeededDate);
	labelItemType.parentNode.removeChild(labelItemType);
        button.parentNode.removeChild(button);
        instructions.innerText = 'The following text has been copied to your clipboard. In Costpoint, create the order header, then create a new line item, and click the dropdown box next to copy, and click Paste From Excel.';
        textData.value = cpData.join('\n');
        
        copyRegion = document.getElementById('csvdata');
        copyRegion.focus();
        copyRegion.select();
        document.execCommand('copy');

    }
    function createReqLines(data, deliverTo, requestedDate, shipID, itemType) {
        console.log('createReqLines');
        var buf = [ [ 'Req Line', 'Item', 'Item Rev', 'Description', 'Line Notes', 'Quantity', 'Req U/M', 'Est Unit Cost Amt', 'Extended Cost Amt', 'Deliver To', 'Requested Data', 'Manufacturer Part', 'Internal Notes', 'Ship ID', 'Manufacturer', 'Inv Abbrev', 'Warehouse', 'Ship Via', 'Status', 'Taxable', 'Sales Tax/VAT Rate', 'Misc Type', 'Est Line Charge Amt', 'Total Est Cost Amt', 'Est Cost Type', 'Est Sales Tax/VAT Amt', 'Cert Conf Reqd', 'QC Insp Reqd', 'Order Reference', 'Line Type', 'Commodity', 'Preferred Vendor Name', 'Mfg Rev', 'Vend Rev', 'Date Approved', 'Buyer Assignment Date', 'Target Place Date', 'Order Ref Type', 'Drop Ship', 'Buyer', 'Preferred Vendor', 'Vendor Part', 'Preferred Quote', 'Suggested Blanket PO', 'Procurement Type', 'Srce Insp Reqd', 'Overshipments Allowed', 'Receipt Tolerance', 'Planner', 'Industry Class', 'Mil-Spec', 'NSN', 'CLIN', 'BOM Configuration', 'Order Ref Release', 'Operation', 'Order Ref Line', 'Performance Start Date', 'Performance End Date', 'Line Rev', 'Delivery Schedule Exists', 'NAICS Code', 'Resource Exists', 'US Citizenship Reqd', 'ITAR Authorization Reqd', 'Security Clearance System ID', 'Security Clearance Description', 'Security Clearance Level', 'SCI', 'SAP', 'Issuing Agency' ].join('\t') ];
        for (let i=0; i < data.length; ++i) {
            if (i == 0) continue; //skip header row
            var line = [data[i][0],
                        itemType, //'G-LAB EQUIPMENT - COMPONENTS',
                        '',
                        data[i][4], //description
                        data[i][5], //line notes
                        data[i][3], //qty
                        'EA',
                        data[i][2], //unit price
                        data[i][6], //ext price
                        deliverTo,
                        requestedDate,
                        data[i][1], //mpn
                        '',         //internal notes
                        shipID,
                        '', '', '', '',
                        'Pending',
                        'No',
                        '0.06',
                        '',
                        0,
                        data[i][6],
                        '',
                        0,
                        'No',
                        'No',
                        '',
                        'G',
                        '', '', '', '', '', '', '',
                        'N',
                        'N',
                        '', '', '', '', '', '',
                        'No',
                        'No',
                        0,
                        '', '', '', '', '', '',
                        0,
                        '',
                        0,
                        '', '',
                        0,
                        'N',
                        '',
                        'N',
                        'N',
                        'N',
                        '', '', '',
                        'N',
                        'N'].join('\t');
            buf.push(line);
        }
        return buf;
    }
})();
